# Description
This program has a REST API endpoint (impl. via [Jersey](https://eclipse-ee4j.github.io/jersey/)) `/api/address` and additionally an ordinary Servlet `/test`. The dependency injection happens via [Weld](https://docs.jboss.org/weld/reference/5.0.1.Final/en-US/html_single/).

When it comes to integration tests in the Maven Build, a Docker image for [Jetty](https://hub.docker.com/_/jetty) is build first, then the corresponding Docker container starts (so the application is deployed in Jetty) and thereafter the integration test executes. 
Finally, the container stops and is removed again until the Maven Build ends completely. 

# Setting
- Java Version: 17
- Build Tool: Apache Maven 3.6.0
- IDE: IDEA intelliJ

# Builds the Docker image, starts the container, starts the integration test and finally stops the container via Maven
```
$> mvn clean verify
```

# Building the image (without the Maven Docker Plugin)
```
$> docker build --tag haase/app-in-jetty:auto -f docker/Dockerfile .
```

# Running the container (without the Maven Docker Plugin)
```
$> docker run -p 80:8080 --rm haase/app-in-jetty:auto
```

# How to run it in the Jetty 11.0.12 (standalone) Servlet Container?
For this the following Jetty modules have to be installed
```
~/jetty-base$> java -jar ~/jetty-home-11.0.12/start.jar --add-module=server,http,deploy,cdi-decorate,annotations
```

# How to run it from your IDE?
```
$> mvn jetty:run
```
