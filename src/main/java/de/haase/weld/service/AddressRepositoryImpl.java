package de.haase.weld.service;

import jakarta.inject.Singleton;

@Singleton
public class AddressRepositoryImpl implements AddressRepository {
    @Override
    public String getStreet(int id) {
        return "Marktstr. 40";
    }

    @Override
    public String getPostcode(int id) {
        return "12345";
    }

    @Override
    public String getCity(int id) {
        return "Berlin";
    }
}
