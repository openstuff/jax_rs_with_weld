package de.haase.weld.service;

public interface AddressRepository {
    String getStreet(int id);
    String getPostcode(int id);
    String getCity(int id);
}
