package de.haase.weld;

import java.math.BigDecimal;

public class Shipping {
    private String type;
    private BigDecimal price;

    public Shipping(String type, BigDecimal price) {
        this.type = type;
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public BigDecimal getPrice() {
        return price;
    }
}
