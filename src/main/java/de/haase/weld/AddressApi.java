package de.haase.weld;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.haase.weld.service.AddressRepository;
import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

@Path("address")
public class AddressApi {

    @Inject
    private AddressRepository addressRepository;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getAddress() {
        return "{\"street\": \"" + addressRepository.getStreet(0) + "\", \"postcode\":  \"" +
                addressRepository.getPostcode(0) + "\", \"city\": \"" + addressRepository.getCity(0) + "\"}";
    }

    @GET
    @Path("/shipping")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getShippingData()  {
        Shipping s1 = new Shipping("Nachtverschickung", new BigDecimal("33.01"));
        Shipping s2 = new Shipping("Schneller Vogel", new BigDecimal("50.03"));
        List<Shipping> result = Arrays.asList(s1, s2);
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return Response.status(200).header("Access-Control-Allow-Origin", "*")
                    .entity(objectMapper.writeValueAsString(result)).build();
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
